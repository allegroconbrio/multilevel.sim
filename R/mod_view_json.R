#' view_json UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd 
#'
#' @importFrom shiny NS tagList htmlOutput
mod_view_json_ui <- function(id) {
  ns <- NS(id)
  tagList(
    htmlOutput(
      outputId = ns('json_out'),
      container = tags$code,
      style = 'white-space: pre-wrap;'
    )
  )
}
    
#' view_json Server Functions
#'
#' @import shiny
#' @importFrom jsonlite toJSON
#' @noRd 
mod_view_json_server <- function(id, config_list) {
  moduleServer( id, function(input, output, session) {
    ns <- session$ns

    output$json_out <- renderUI( {
      if(is.reactive(config_list)) {
        config_list <- config_list()
      }
      toJSON(config_list,
             simplifyVector = TRUE,
             simplifyDataFrame = FALSE,
             simplifyMatrix = TRUE,
             pretty = TRUE,
             auto_unbox = TRUE)
    } )
  } )
}
    
## To be copied in the UI
# mod_view_json_ui("view_json_ui_1")
    
## To be copied in the server
# mod_view_json_server("view_json_ui_1")
