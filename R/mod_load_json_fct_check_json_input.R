#' check_json_input
#' @importFrom  purrr map safely
#' @param input_list a list representing a JSON.
#' @description Check an input list for conformity.
#' @returns A list with one element for each element in `input_list`.
#'          if the elements comply with the template, the `$error` field
#'          for each element is empty.  If some object does not comply,
#'          you will find an `$error` appended.
check_json_input <- function(input_list) {
  map(TYPES_GLOBAL, safely(check_item), input = input_list)
}
