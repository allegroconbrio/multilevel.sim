#' covariates UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd 
#'
#' @importFrom shiny NS tagList sliderInput numericInput
#' @import shinyMatrix
mod_covariates_ui <- function(id) {
  ns <- NS(id)
  if (Sys.getenv('R_CONFIG_ACTIVE') == 'shinyapps') {
    max_covariates <- 3
  } else {
    max_covariates <- 10
  }
  tagList(
    numericInput(ns('num_intercept'), label = 'Intercept', value = 1),
    sliderInput(ns('sli_n'), min = 1, max = max_covariates,
                label = 'Number of Variables',
                value = 1),
    matrixInput(ns('mat_sigma'), value = matrix(1, dimnames = list('v1', 'v1')),
                label = 'Sigma', class = 'numeric'),
    matrixInput(ns('mat_mean'), value = matrix(0, dimnames = list(NULL, 'v1')),
                label = 'Means', class = 'numeric', rows = list(names = FALSE))
  )
}
    
#' covariates Server Functions
#'
#' @noRd 
#' @importFrom shinyMatrix updateMatrixInput
mod_covariates_server <- function(id) {
  moduleServer( id, function(input, output, session){
    ns <- session$ns
    observeEvent(input$sli_n, {
      nam <- paste0('v', 1:input$sli_n)
      mini <- min(input$sli_n, nrow(input$mat_sigma))
      old_value <- input$mat_sigma[1:mini, 1:mini]
      new_value <- diag(input$sli_n)
      new_value[1:mini, 1:mini] <- old_value
      dimnames(new_value) <- list(nam, nam)
      updateMatrixInput(session = session, inputId = 'mat_sigma',
                        value = new_value)
      old_value <- input$mat_mean[, 1:mini]
      new_value <- t(rep(0, input$sli_n))
      new_value[, 1:mini] <- old_value
      colnames(new_value) <- nam
      updateMatrixInput(session = session, inputId = 'mat_mean',
                        value = new_value)
    } )
    return(reactive( {
      list(
        distribution = 'multinorm',
        parameters = list(
          sigma = input$mat_sigma,
          mean = input$mat_mean
        ),
        labels = colnames(input$mat_sigma)
      )
    } ))
  })
}
    
## To be copied in the UI
# mod_covariates_ui("covariates_ui_1")
    
## To be copied in the server
# mod_covariates_server("covariates_ui_1")
