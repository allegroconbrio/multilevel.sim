#' @importFrom stats pbeta 
get_group_cutoffs <- function(n_groups, alpha, beta) {
  # return a vector of length n_groups with values between 0 and 1
  # if `alpha` == `beta` == 1, then all values will be evenly spaced.
  # alpha and beta are parameters to the beta distribution,
  # so as long as alpha == beta, the distribution is symmetrical,
  # and so will the cutoff values be.
  # alpha and beta can be tweaked so that the distribution may
  # be more asymmetrical
  pbeta(1:n_groups/n_groups, shape1 = alpha, shape2 = beta)
}

get_group_sizes <- function(n_samples, cutoffs) {
  # return a vector of integers
  tmp <- round(diff(n_samples * cutoffs))
  c(tmp, n_samples - sum(tmp))
}
