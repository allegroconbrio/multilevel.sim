#' compute_fixef_confints
#' @title Compute confidence intervals for a list of simulation runs in a
#'        data.table
#' @param simulation a list of simulation runs, the direct output of
#'        `run_simulation`.
#' @param idcol a character which names the column which enumerates the
#'        simulation runs.  Default is "..simulation_run".
#' @param ... Arguments passed to `confint.merMod`
#' @param progress A character vector:
#'        - `"txt"` for a character based progress bar on the console
#'        - `"shiny"` for a progress bar in the Shiny web interface
#'        - `"none"` or NULL for not progress indication at all (default)
#' @param session Only in use when `progress = 'shiny'`.  Then you must supply
#'        the shiny session object.
#' @returns a `data.table`
#' @importFrom purrr map safely imap
#' @importFrom lme4 confint.merMod
#' @importFrom data.table setattr setnames as.data.table rbindlist
#' @importFrom shiny incProgress Progress
#' @description Compute the confidence intervals for each parameter in each
#'              simulation.
compute_fixef_confints <- function(simulation, idcol = '..simulation_run', ...,
                                   progress = 'none', session = NULL) {
  if (progress == 'shiny' & is.null(session)) {
    stop('`progress` is set to "shiny"` but no `session` has been supplied')
  }
  arglist <- list(...)
  confint_func <- function(x) {
    suppressMessages(
      confints <- do.call(what = safely(confint.merMod),
                          args = append(list(object = x$model), arglist))
    )
    if (!is.null(confints$error))
      warning(paste('Error in `confint.merMod`:\n\n',confints$error))
    return(confints)
  }
  progress_wrapper <- switch(
    progress,
    shiny = function(func) {
      shinyprogress <- shiny::Progress$new(
        session = session,
        min = 0,
        max = attr(simulation, 'template')$n_iterations
      )
      shinyprogress$set(message = 'Computing Confidence Intervals...')
      function(x) {
        shinyprogress$inc(
          amount = 1,
          detail = paste0(shinyprogress$getValue(), '/', shinyprogress$getMax())
        )
        if (shinyprogress$getMax() == shinyprogress$getValue()) {
          shinyprogress$close()
        }
        return(func(x))
      }
    } ,
    txt = function(func) {
      pb <- txtProgressBar(min = 0, max = length(simulation))
      function(x) {
        setTxtProgressBar(pb = pb, value = getTxtProgressBar(pb) + 1)
        return(func(x))
      }
    },
    none = identity
  )
  message('Computing confidence intervals for fixed effects...')
  confints <- map(
    .x = simulation,
    .f = progress_wrapper(confint_func)
  )
  out <- rbindlist(
    map(confints, ~ as.data.table(.x$result, keep.rownames = TRUE)),
    idcol = idcol
  )
  errors <- discard(imap(
    confints,
    ~ if (!is.null(.x$error)) append(as.list(.x$error), list(..simulation_run = .y))
  ), .p = is.null)
  setattr(out, name = 'errors', value = errors)
  setnames(out, 'rn', 'parameter')
  setnames(out, 3:4, new = c('lower', 'upper'))
  level <- if(is.null(arglist$level)) .95 else arglist$level
  setattr(out, 'level', level)
  return(out)
}

#' make_coverage_fixef_confints
#' @title Compute confidence intervals and coverages of fixed effects
#'        for a simulation
#' @description Compute the coverages of fixed effects
#'              and return a `data.table` combine them with the true values
#'              to receive a coverage with `make_ranef_coverages`.
#' @importFrom purrr map map_lgl
#' @importFrom data.table data.table merge.data.table %between% set
#' @importFrom lme4 isSingular
make_coverage_fixef_confints <- function(simulation,
                                         idcol = '..simulation_run',
                                         ...,
                                         progress = 'none',
                                         session = NULL) {
  arglist <- list(...)
  arglist[c('simulation', 'idcol', 'oldNames', 'progress', 'session')] <-
    list(simulation, idcol, FALSE, progress, session)
  # always use `oldNames = FALSE` for compatibility with extraction functions
  confints <- do.call(what = compute_fixef_confints, args = arglist)
  template <- attr(simulation, 'template')
  true_beta <- data.table(parameter = names(template$beta),
                          true_value = unlist(template$beta))
  merged <- merge.data.table(confints, true_beta, by = 'parameter', all.x = TRUE)
  is_singular_fit <- map_lgl(simulation, ~ isSingular(.x$model))
  set(
    x = merged,
    j = 'is_singular_fit',
    value = is_singular_fit[merged[[idcol]]]
  )
  true_sigma <- DISTRIBUTIONS[[template$epsilon$distribution]]$sigma(template$epsilon)
  with(data = merged, expr = {
    set(merged, i = which(parameter == 'sigma'), j = 'true_value', value = true_sigma)
  } )
  with(data = merged, expr = {
    set(merged, j = 'is_true_value_covered', value = true_value %between% list(lower, upper))
  })
  setattr(merged, 'template', attr(simulation, 'template'))
  setattr(merged, 'errors', attr(confints, 'errors'))
  setattr(merged, 'level', attr(confints, 'level'))
  setattr(merged, 'class', c('mlSimFixefOutput', class(merged)))
  return(merged)
}

#' plot.mlSimFixefOutput
#' @title plot the coverages of fixed effects
#' @describeIn compute_fixef_confints  `plot.mlSimFixefOutput` plots the
#'             coverages of fixed effects
#' @param confints The direct output of `make_coverage_fixef_confints`
#' @return a ggplot
#' @import ggplot2
#' @importFrom data.table melt.data.table
plot.mlSimFixefOutput <- function(confints) {
  ggplot(data = confints, aes(x = parameter, fill = is_true_value_covered)) +
    geom_bar(stat = 'count', position = 'fill') +
    coord_flip() +
    scale_y_continuous(name = 'Percentage of runs', labels = percent) +
    scale_fill_manual(values = c('FALSE' = '#E41A1C', 'TRUE' = '#377EB8')) +
    labs(title = 'Coverage of Fixed Effects and Standard Deviations/Correlations') +
    theme_minimal()
}

#' plot_fixef_samples
#' @title Plot a sample of simulations, the true parameter values
#'        and the coverage interval for each simulation.
#' @importFrom data.table data.table setkey uniqueN
#' @description These functions draw the true values of either the parameters
#'              or the random effects of each simulation and their respective
#'              confidence interval (in case of fixed effects) or posterior
#'              interval (in case of random effects).
#'              This is very useful if you want to see the extent by which
#'              the different intervals vary, or how far the different true
#'              values (in case of random effects) lie apart.
#' @param coverages For `plot_fixef_samples` the direct output of
#'        `make_coverage_fixef_confints`, for `plot_ranef_sample` the direct
#'        output of `make_ranef_coverages`.
#' @param alpha Alpha (opacity) of the lines drawn
#' @param sample The number of simulations to plot.
#' @param seed Integer, a seed to make the plot reproducible.
#' @param title Logical (default `TRUE`), should a title be given to the plot?
#' @param caption Logical (default `TRUE`), should a caption with information
#'                be provided?
#' @import ggplot2
plot_fixef_samples <- function(coverages, sample = 20, alpha = 1,
                               title = TRUE, caption = TRUE) {
  if (!('mlSimFixefOutput' %in% class(coverages))) {
    stop('`coverages` is not an object of class "mlSimFixefOutput"')
  }
  sample <- coverages[, min(max(..simulation_run), sample)]
  idx <- data.table(
    ..simulation_run = sample(max(coverages$..simulation_run), size = sample),
    key = '..simulation_run'
  )
  template <- attr(coverages, 'template')
  setkey(coverages, ..simulation_run)
  plotdata <- coverages[idx][!grepl('groups', parameter)][
    order(parameter, is_true_value_covered)
  ]
  hlinedata <- unique(plotdata[, .(true_value, parameter)])
  hlinedata[, helper := 1:.N]
  plt <- ggplot(mapping = aes(x = parameter, y = true_value)) +
    geom_segment(data = hlinedata,
                 mapping = aes(x = helper - .45, xend = helper + .45,
                               y = true_value, yend = true_value)) +
    scale_x_discrete() +
    geom_errorbar(
      data = plotdata,
      mapping = aes(ymin = lower, ymax = upper, colour = is_true_value_covered,
                    group = ..simulation_run),
      position = position_dodge(width = .8),
      width = .03,
      alpha = alpha) +
    scale_colour_manual(values = c('FALSE' = '#E41A1C', 'TRUE' = '#377EB8'),
                        name = 'True Value Covered') +
    xlab('Parameter') + ylab('Value') +
    theme_minimal() +
    theme(legend.position = 'bottom')
  if (caption) {
    plt <- plt +
      labs(caption = paste(
        'Simulations shown: ', sample, ', ',
        'Total Simulations: ', template$n_iterations, ', ',
        'Failed simulations: ', template$n_iterations - coverages[, uniqueN(..simulation_run)]
      ))
  }
  if (title) {
    plt <- plt + ggtitle('Coverages of Parameter Estimates for Each Simulation')
  }
  return(plt)
}
