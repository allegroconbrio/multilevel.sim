#' Inverted versions of in, is.null and is.na
#' 
#' @noRd
#' 
#' @examples
#' 1 %not_in% 1:10
#' not_null(NULL)
`%notin%` <- Negate(`%in%`)

notnull <- Negate(is.null)

notna <- Negate(is.na)

#' Removes the null from a vector
#' 
#' @noRd
#' 
#' @example 
#' drop_nulls(list(1, NULL, 2))
drop_nulls <- function(x){
  x[!sapply(x, is.null)]
}

#' If x is `NULL`, return y, otherwise return x
#' 
#' @param x,y Two elements to test, one potentially `NULL`
#' 
#' @noRd
#' 
#' @examples
#' NULL %||% 1
"%||%" <- function(x, y){
  if (is.null(x)) {
    y
  } else {
    x
  }
}

#' If x is `NA`, return y, otherwise return x
#' 
#' @param x,y Two elements to test, one potentially `NA`
#' 
#' @noRd
#' 
#' @examples
#' NA %||% 1
"%|NA|%" <- function(x, y){
  if (is.na(x)) {
    y
  } else {
    x
  }
}

#' Typing reactiveValues is too long
#' 
#' @inheritParams reactiveValues
#' @inheritParams reactiveValuesToList
#' 
#' @noRd
rv <- shiny::reactiveValues
rvtl <- shiny::reactiveValuesToList


#' convenience function, like data.table's CJ
#' @title Generate all possible combinations of elements of vectors
#' @description This function works like `data.table`'s `CJ`. but returns
#'              a `matrix` rather than a `data.table`.  You can pass a number
#'              of vectors, and every possible combination of the elements in 
#'              these vectors will be returned as a matrix.  You can also just
#'              return the indices of these vectors, rather than their values
#'              (using `along = TRUE`).
#'              This is useful to avoid different data types being combined
#'              to a single data type.
#' @param ... Vectors
#' @param along Boolean, should the indices of the vectors be returned?
crossjoin <- function(..., along = TRUE) {
  args <- list(...)
  single_indices <- lapply(lapply(args, seq_along), matrix)
  wrapper <- function(x, y) {
    cbind(
      x[rep(seq_len(nrow(x)), rep(nrow(y), nrow(x))), ],
      y[rep(y, nrow(x)), ]
    )
  }
  joint_indices <- reduce(single_indices, wrapper)
  if (along)
    return(joint_indices)
  return(
    do.call(cbind, lapply(1:length(args), function(idx) args[[idx]][joint_indices[, idx]]))
  )
}
