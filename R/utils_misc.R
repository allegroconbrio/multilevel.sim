#' example_simulation
#'
#' @description A sample configuration for a simulation.
#'
#' @return A list representing a `{multilevel.sim}` conforming simulation
#'         configuration.
#'
#' @export
example_sim <- function() {
  return(
    list(
      m = 100,
      J = 3,
      group_sizes = c(33, 33, 34),
      covariates = list(
        list(
          distribution = 'identity',
          parameters = list(
            'x' = 1
          ),
          labels = 'Intercept'
        )
      ),
      beta = c('Intercept' = 100),
      fixed_effects = c('Intercept'),
      random_effects = list(
        list(
          distribution = 'normal',
          parameters = list(
            mean = 0,
            sd = 10
          ),
          variables = 'Intercept'
        )
      ),
      epsilon = list(
        distribution = 'normal',
        parameters = list(
          mean = 0,
          sd = 15
        )
      ),
      n_iterations = 100,
      seed = 123,
      constant_ranefs = FALSE,
      run_lm = TRUE
    )
  )
}
