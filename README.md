
<!-- README.md is generated from README.Rmd. Please edit that file -->

# multilevel.sim

<!-- badges: start -->

[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://lifecycle.r-lib.org/articles/stages.html#experimental)
<!-- badges: end -->

The goal of multilevel.sim is to offer a simple interface for simulating
and evaluating *multilevel models* (MLM) of the form

    y = Xb + Zd + e

The simplicity of `{multilevel.sim}` is that you can define a simulation
config which contains all the necessary hyperparameters for this
simulation.

## Installation

You can install the latest version of `{multilevel.sim}` using the
[devtools](https://github.com/r-lib/devtools) package with:

``` r
devtools::install_gitlab('username/multilevel.sim')
```

## Example

This is a basic example which shows you how to run a simple simulation.
Run a simulation with an example configuration and extract all
information about the random effects like so:

``` r
library(multilevel.sim)
simulation_settings <- example_sim()
simulation <- mlm_simulation(template = simulation_settings)
#> ================================================================================
ranef_coverage <- make_ranef_coverages(simulation)
```

Plot the coverage of all variables in the 3 groups:

``` r
plot(ranef_coverage)  # coverage plot over all iterations
```

<img src="man/figures/README-example-plot-ranef-coverage-1.png" width="100%" />
Plot the conditional values with their conditional variances for each
random effects variable and for each group in every simulation iteration
with this command:

``` r
plot_ranef_sample(ranef_coverage)  # plot the random effects of each iteration
```

<img src="man/figures/README-example-plot-ranef-sample-1.png" width="100%" />
Above, each point is a conditional value for a variable of a group in
one specific simulation iteration. Hence this plot gives you an
impression of the entire simulation, giving you the possibility to see
the variation between the simulation iterations.

## Instructions

### The Statistical Model Under Investigation

`{multilevel.sim}` generates data and estimates models according to the
model formula

    y = Xb + Zd + e

, where **y** is a column vector, **X** is a matrix of observed
covariates, **b** is a column vector of parameters, **Z** is a matrix of
observed covariates partitioned by a group structure, which is also
known. Finally, e is a column vector of error terms. The data is
clustered in **J** groups, and the total number of data points is **m**.
For a simulation, all of these terms must be generate data for which
`{multilevel.sim}` has a config structure that can be expressed in R as
a structure of objects nested in lists, but also in JSON format.

Here is a short description of how each term is defined by the objects
in the simulation configuration:

  - **X** and **Z** are generated according to the definitions in
    `"covariates"`
  - the **group partitioning** of **Z** is defined by the values given
    in `"group_sizes"`, `"J"` and `"m"`
  - `"m"` and `"J"` are sort of redundant because the length of
    `"group_sizes"` must be equal to `"J"` and the sum of the values in
    `"group_sizes"` must be equal to `"m"`, but at this moment, `"m"`
    and `"J"` must be supplied nevertheless.
  - **b** is defined by the values in `"beta"`
  - **d** is defined by the definitions in `"random_effects"`.
  - For **X**, **Z** and **e**, one value (row) per individual data
    point is drawn from the respective definitions.
  - For **d**, one value per group and random-effect variable is drawn
    from the definitions.

The notation has its inspiration from Mejer and de Leeuw (2008) \[1\]
and Bates (2015) \[2\], and it is not be explained any further here,
mainly because I cannot render math and formula symbols in this
README.md. Please refer to these sources for further details.

`{multilevel.sim}` uses a defined config structure to perform multilevel
simulalations. The config is JSON compatible, but can also be defined in
R as a list structure right away. Nested JSON dictionaries (objects) are
interpreted as lists in R. JSON arrays are vectors in R, and a JSON
dictionary that has no other dictionaries among its elements is
interpreted as a named vector in R. Two-dimensional JSON arrays (where
each ) are interpreted as matrices in R.

### The Simulation Config

Below you see the structure and data types of the simulation config.
Three dots mean that further objects like the previous can follow in the
array. Terms in `<` and `>` state the data type of the object that is
expected.

``` json
{
  "m": <integer>,
  "J": <integer>,
  "group_sizes": [<integer>, ...],
  "covariates": [
      {
          "distribution": <string>,
          "parameters": <named list / dictionary>,
          "labels": <array of strings>
      },
      ...
  ],
  "beta": <named vector or list / dictionary>,
  "fixed_effects": <array of strings>,
  "random_effects": [
      {
          "distribution": <string>,
          "parameters": <named list / dictionary>,
          "variables": [<string>, ...]
      },
      ...
  ],
  "epsilon": {
      "distribution": <string>,
      "parameters": <named list / dictionary>
  }
  "n_iterations": <integer>,
  "constant_ranefs": <boolean>,
  "seed": <integer>,
  "run_lm": <boolean>
}
```

  - `"m"`: The number of total individual samples
  - `"J"`: The number of groups
  - `"group_sizes"`: An array of integers giving the sizes of each
    group.
  - `"covariates"`: An array of objects defining one or many
    *distribution definitions*, see below.
  - `"fixed_effects"`: An array labels of the variables which are to be
    estimated as fixed effects in each simulation iteration. Must be
    labels present in the `"labels"` arguments of the `"covariates"`.
  - `"beta"`: A named vector or list (in R), or a dictionary (in JSON),
    whose names must be characters given in `"labels"` argument of
    `"covariates"`, and whose values are the true fixed effects.
  - `"random_effects"`: An unnamed list (in R) or an array (in JSON) of
    *distribution definitions* (see below).
  - `"epsilon"`: A single *distribution definition*.
  - `"n_iterations"`: An integer giving the number of simulations to
    perform.
  - `"seed"`: An integer stating the random seed. The same seed with the
    same config always yields the same results.
  - `"run_lm"`: boolean, should a linear model also be run? A linear
    model treats each group as a separate fixed effect and allows to
    compare how random effects models would estimate group values in
    comparison to fixed effects in a normal linear model.

Possible elements for `"covariates"` and `"random_effects"` are
*distribution definitions*. The only possible distributions at this
moment are the `"normal"`, `"multinorm"` and `"identity"`. The first two
define a normal, multivariate normal distribution, respectively. The
“identity” distribution simply represents a constant and is usually
used to implement an intercept with the value 1. It can be used for
atypical intercepts different from 1 if desired.

### Distribtion definitions

The *distribution definitions* are named lists (in R) or objects (in
JSON) and need the following named elements:

  - `"distribution"`: a single character value, one of
      - “normal”
      - “multinorm”
      - “identity”
  - `"parameters"`: a named list (in R) or an object (in JSON) giving
    all the required parameters for the given distribution. See the
    *distribution parameters* documentation below.
  - `"labels"` (for `covariates"`) / `"variables"` (for
    `"random_effects"`)
      - A character vector (in R) or an array of characters (in JSON).
      - This character vector must have as many elements as the
        distribution definition returns data columns. The “identity” and
        “normal” distributions return exactly one data column and must
        hence have exactly a single “label” element. The “multinorm”
        distribution generates as many data columns as there are
        diagonal elements in its `"sigma"` argument, and must have as
        many elements in `"labels"`.
      - This character vector is ordered, i. e., the first data column
        generated by this distribution definition will have the first
        element of this character vector as label, and so on.
      - All elements must be unique.
      - `"labels"` (for covariates):
          - This character vector gives labels to the variables which
            are to be generated by this definition. These labels are
            important because they are referenced by the `"variables"`
            field of the `"random_effects"` definition (below). Also,
            they are displayed in the output summaries and graphics.
      - `"variables"` (for `"random_effects"`)
          - These character elements **must** refer to labels given in
            the distribution definitions of the `"covariates"` element.
            They define onto which variable which random effect is laid.
  - A distribution definition given in `"epsilon"` does not have
    `"labels"` nor `"variables"`.

#### Parameters for the different distribution definitions

There are currently three available distribution types: `"normal"`,
`"multinorm"`, and "`identity"`. See the list below for descriptions and
the parameters they take:

  - `"identity"`
      - **Description**: Creates a single constant value. Usually used
        for creating a constant column of ones in the data for
        intercept.
      - **Parameters**:
          - `"x"`: A single numeric, the constant you want to create.
  - `"normal"`
      - **Description**: Creates a single column of normally distributed
        values. This definition is analogous to R’s `rnorm` function.
      - **Parameters**:
          - `"mean"`: A single numeric value defining the mean of the
            data to be generated.
          - `"sd"`: A single numeric value defining the standard
            deviation of the data to be generated. *Note that this is
            the standard deviation, whereas `"multinorm"`’s* *`"sigma"`
            takes the variance.*
  - `"multinorm"`
      - **Description**: Creates several columns of data from a
        multivariate normal distribution, given a variance-covariance
        matrix `"sigma"` and a vector of means (`"means"`). Generates as
        many data columns as there are elements in the diagonal of
        `"sigma"`. This is analogous to R’s `mvtnorm::rmvnorm` function.
      - **Parameters**:
          - `"sigma"`: A square numeric matrix (in R) or a
            two-dimensional array of numerics (in JSON), whose diagonal
            must be non-negative. The diagonal of this matrix must have
            as many elements as there are elements in the `"means"`
            argument.
          - `"means"`: A numeric vector (in R) or an array of numerics
            (in JSON) which gives the means for each variable column to
            be generated. Must have as many elements as there are
            elements in the diagonal of `"sigma"`

### Function Reference

`{multilevel.sim}` exports the following functions:

  - `example_sim` to give you an example of a simulation config that you
    can pass right to `mlm_simulation`
  - `make_ranef_coverages` which takes the output of `mlm_simulation`
    and computes coverages and information about the random effects
    estimated
  - `mlm_simulation`, which takes the above described simulation config
    and performs the simulation
  - `plot_ranef_sample` to plot the random effects of a sample of
    simulation iterations
  - `run_app`, which starts a `{shiny}` application to use the package
    graphically. Experimental.

Please refer to the function documentation in R. More functions are in
development.

1.  Leeuw, Jan de, and Erik Meijer. 2008. “Introduction to Multilevel
    Analysis.” In *Handbook of Multilevel Analysis*, edited by Jan de
    Leeuw and Erik Meijer.

2.  Bates, Douglas, Martin Mächler, Ben Bolker, and Steve Walker. 2015.
    “Fitting Linear Mixed-Effects Models Using lme4.” *Journal of
    Statistical* Software 67 (1): 1–48.
    <https://doi.org/10.18637/jss.v067.i01>.
