test_that("variable def identity", {
  variable_def <- list(
    distribution = 'identity',
    parameters = list(
      x = 1
    ),
    labels = 'v0'
  )
  expect_type(check_variable_def(variable_def), type = 'list')
})

test_that("covariate def identity throws error for wrong label", {
  covariate_def <- list(
    distribution = 'identity',
    parameters = list(
      x = 1
    ),
    labels = NULL
  )
  expect_error(check_covariate_def(covariate_def), regexp = '`labels`')
})

test_that("variable def throws error for wrong distribution", {
  variable_def <- list(
    distribution = 'wrong_distribution_name',
    parameters = list(
      x = 1
    ),
    labels = 'v0'
  )
  expect_error(check_variable_def(variable_def), regexp = 'distribution')
})


test_that("variable def multinorm fails for missing sigma", {
  variable_def <- list(
    distribution = 'multinorm',
    parameters = list(
      x = 1
    ),
    labels = 'v0'
  )
  expect_error(check_variable_def(variable_def), regexp = 'sigma')
})

test_that("variable def multinorm", {
  variable_def <- list(
    distribution = 'multinorm',
    parameters = list(
      sigma = diag(2),
      mean = c(0, 0)
    ),
    labels = c('v0', 'v1')
  )
  expect_type(check_variable_def(variable_def), type = 'list')
})

test_that("variable def multinorm runs without mean specified", {
  variable_def <- list(
    distribution = 'multinorm',
    parameters = list(
      sigma = diag(2)
    ),
    labels = c('v0', 'v1')
  )
  expect_type(check_variable_def(variable_def), type = 'list')
})

test_that('ranef_def throws error for wrong variables', {
  variable_def <- list(
    distribution = 'multinorm',
    parameters = list(
      sigma = diag(2),
      mean = c(0, 0)
    ),
    variables = c('v0')
  )
  expect_error(check_ranef_def(variable_def), regexp = '`variables`')
})

test_that('check_ranef_def multinorm success', {
  ranef_def <- list(
    distribution = 'multinorm',
    parameters = list(
      sigma = diag(2),
      mean = c(0, 0)
    ),
    variables = c('v0', 'v1')
  )
  expect_true(check_ranef_def(ranef_def))
})

test_that('check_ranef_def identity success', {
  ranef_def <- list(
    distribution = 'identity',
    parameters = list(
      x = 1
    ),
    variables = c('v0')
  )
  expect_true(check_ranef_def(ranef_def))
})

test_that('check_covariate_def identity success', {
  covariate_def <- list(
    distribution = 'identity',
    parameters = list(
      x = 1
    ),
    labels = c('v0')
  )
  expect_true(check_covariate_def(covariate_def))
})

test_that('check_covariate_def throws error for missing labels', {
  covariate_def <- list(
    distribution = 'identity',
    parameters = list(
      x = 1
    ),
    variables = c('v0')  # accidentally passing `variables` instead of labels
  )
  expect_error(check_covariate_def(covariate_def), regexp = '`labels` is missing')
})

test_that('check_ranef_def throws error for missing variables', {
  ranef_def <- list(
    distribution = 'identity',
    parameters = list(
      x = 1
    ),
    labels = c('v0')  # accidentally passing `labels` instead of variables
  )
  expect_error(check_ranef_def(ranef_def), regexp = '`variables` is missing')
})

test_that('check_item for `m`', {
  item <- TYPES_GLOBAL[['m']]
  input <- list(
    m = 1000
  )
  expect_true(check_item(item, input))
} )

test_that('check_item throws error for missing `m`', {
  item <- TYPES_GLOBAL[['m']]
  input <- list(
    
  )
  expect_error(check_item(item, input), regexp = '`m`')
} )

test_that('check_item for `J`', {
  item <- TYPES_GLOBAL[['J']]
  input <- list(
    J = 10
  )
  expect_true(check_item(item, input))
})

test_that('check_item fails for `J` missing', {
  item <- TYPES_GLOBAL[['J']]
  input <- list()
  expect_error(check_item(item, input), regexp = '`J`')
} )

test_that('check_item for `group_sizes`', {
  item <- TYPES_GLOBAL[['group_sizes']]
  input <- list(
    m = 300,
    J = 3,
    group_sizes = c(100, 100, 100)
  )
  expect_true(check_item(item, input))
} )

test_that('check_item fails for `group_sizes` not summing up to `m`', {
  item <- TYPES_GLOBAL[['group_sizes']]
  input <- list(
    m = 300,
    J = 3,
    group_sizes = c(100, 100, 200)
  )
  expect_error(check_item(item, input), regexp = '`m`')
} )

test_that('check_item fails for `length(group_sizes)` not equal to `J`', {
  item <- TYPES_GLOBAL[['group_sizes']]
  input <- list(
    m = 300,
    J = 2,
    group_sizes = c(100, 100, 100)
  )
  expect_error(check_item(item, input), regexp = 'group_sizes')
} )

test_that('check_items for covariates', {
  item <- TYPES_GLOBAL[['covariates']]
  input <- list(
    m = 300,
    J = 3,
    group_sizes = c(100, 100, 100),
    covariates = list(
      list(
        distribution = 'multinorm',
        parameters = list(
          mean = c(0, 0),
          sigma = diag(2)
        ),
        labels = c('v0', 'v1')
      )
    )
  )
  expect_true(check_item(item, input))
} )

test_that('check_items fails for missing covariates', {
  item <- TYPES_GLOBAL[['covariates']]
  input <- list(
    m = 300,
    J = 3,
    group_sizes = c(100, 100, 100)
  )
  expect_error(check_item(item, input), regexp = '`covariates`')
} )

test_that('check_item for `run_lm`', {
  item <- TYPES_GLOBAL[['run_lm']]
  input <- list(
    run_lm = FALSE
  )
  expect_true(check_item(item, input))
  input$run_lm <- 'some_string'
  expect_error(check_item(item, input), regexp = 'is.logical')
  input$run_lm <- 777
  expect_error(check_item(item, input), regexp = 'is.logical')
} )


test_that('check_item for random effects', {
  item <- TYPES_GLOBAL[['random_effects']]
  input <- list(
    m = 300,
    J = 3,
    group_sizes = c(100, 100, 100),
    covariates = list(
      list(
        distribution = 'identity',
        parameters = list(
          x = 1
        ),
        labels = 'v0'
      )
    ),
    fixed_effects = c('v0'),
    random_effects = list(
      list(
        distribution = 'multinorm',
        parameters = list(
          mean = c(0),
          sigma = diag(1)
        ),
        variables = c('v0')
      )
    )
  )
  expect_true(check_item(item, input))
} )

test_that('check_item for epsilon', {
  item <- TYPES_GLOBAL[['epsilon']]
  input <- list(
    m = 300,
    J = 3,
    group_sizes = c(100, 100, 100),
    covariates = list(
      list(
        distribution = 'identity',
        parameters = list(
          x = 1
        ),
        labels = 'v0'
      )
    ),
    random_effects = list(
      list(
        distribution = 'multinorm',
        parameters = list(
          mean = c(0),
          sigma = diag(1)
        ),
        variables = c('v0')
        )
      ),
    epsilon = list(
      distribution = 'normal',
      parameters = list(
        mean = 0,
        sd = 1
      )
    )
  )
  expect_true(check_item(item, input))
} )

test_that('check_item for `fixed_effects`', {
  item <- TYPES_GLOBAL[['fixed_effects']]
  input <- list(
    m = 300,
    J = 3,
    group_sizes = c(100, 100, 100),
    covariates = list(
      list(
        distribution = 'identity',
        parameters = list(
          x = 1
        ),
        labels = 'v0'
      )
    ),
    fixed_effects = 'v0'
  )
  expect_true(check_item(item, input))
} )

test_that('check_item fails for fixed_effects not in covariates', {
  item <- TYPES_GLOBAL[['fixed_effects']]
  input <- list(
    m = 300,
    J = 3,
    group_sizes = c(100, 100, 100),
    covariates = list(
      list(
        distribution = 'identity',
        parameters = list(
          x = 1
        ),
        labels = 'v0'
      )
    ),
    fixed_effects = c('some_wrong_label')
  )
  expect_error(check_item(item, input), regexp = 'fixed_effects')
} )

test_that('check_item for beta', {
  item <- TYPES_GLOBAL[['beta']]
  input <- list(
    m = 300,
    J = 3,
    group_sizes = c(100, 100, 100),
    covariates = list(
      list(
        distribution = 'identity',
        parameters = list(
          x = 1
        ),
        labels = 'v0'
      )
    ),
    fixed_effects = list(
      'v1'
    ),
    beta = list(
      v1 = 100
    ),
    random_effects = list(
      list(
        distribution = 'multinorm',
        parameters = list(
          mean = 0,
          sigma = diag(1)
        ),
        variables = 'v0'
      )
    )
  )
  expect_true(check_item(item, input))
} )


test_that('check_item fails for beta not in `fixed_effects`', {
  item <- TYPES_GLOBAL[['beta']]
  input <- list(
    m = 300,
    J = 3,
    group_sizes = c(100, 100, 100),
    covariates = list(
      list(
        distribution = 'identity',
        parameters = list(
          x = 1
        ),
        labels = 'v0'
      )
    ),
    fixed_effects = list(
      'v1'
    ),
    beta = list(
      wrong_label = 100
    ),
    random_effects = list(
      list(
        distribution = 'multinorm',
        parameters = list(
          mean = 0,
          sigma = diag(1)
        ),
        variables = 'v0'
      )
    )
  )
  expect_error(check_item(item, input), regexp = 'fixed_effects')
} )

test_that('check_item fails for `variables` not in fixed_effects', {
  item <- TYPES_GLOBAL[['random_effects']]
  input <- list(
    m = 300,
    J = 3,
    group_sizes = c(100, 100, 100),
    covariates = list(
      list(
        distribution = 'identity',
        parameters = list(
          x = 1
        ),
        labels = 'v1'
      )
    ),
    fixed_effects = list(
      'v1'
    ),
    beta = list(
      v1 = 100
    ),
    random_effects = list(
      list(
        distribution = 'multinorm',
        parameters = list(
          mean = 0,
          sigma = diag(1)
        ),
        variables = 'v0'
      )
    )
  )
  expect_error(check_item(item, input), regexp = 'fixed_effects')
} )

test_that('check_json_input', {
  input <- list(
    m = 300,
    J = 3,
    group_sizes = c(100, 100, 100),
    covariates = list(
      list(
        distribution = 'identity',
        parameters = list(
          x = 1
        ),
        labels = 'v1'
      )
    ),
    fixed_effects = list(
      'v1'
    ),
    beta = list(
      v1 = 100
    ),
    random_effects = list(
      list(
        distribution = 'multinorm',
        parameters = list(
          mean = 0,
          sigma = diag(1)
        ),
        variables = 'v0'
      )
    ),
    constant_ranefs = FALSE
  )
  checks <- check_json_input(input)
  res <- map_lgl(checks, ~ is.null(.x$error))
  expect_identical(
    res,
    c(m = TRUE, J = TRUE, group_sizes = TRUE, covariates = TRUE,
      fixed_effects = TRUE, beta = TRUE,
      random_effects = FALSE,  # 'v0' not in covariates
      epsilon = FALSE,  # missing
      seed = FALSE,  #missing
      n_iterations = FALSE,  # missing
      constant_ranefs = TRUE,
      run_lm = FALSE  # missing
      )
    )
  input$constant_ranefs <- NULL  # now missing
  checks <- check_json_input(input)
  expect_true(!is.null(checks$constant_ranefs$error))
} )
