% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/mod_output_utils_fixefs.R
\name{compute_fixef_confints}
\alias{compute_fixef_confints}
\alias{plot.mlSimFixefOutput}
\title{Compute confidence intervals for a list of simulation runs in a
       data.table}
\usage{
compute_fixef_confints(
  simulation,
  idcol = "..simulation_run",
  ...,
  progress = "none",
  session = NULL
)

\method{plot}{mlSimFixefOutput}(confints)
}
\arguments{
\item{simulation}{a list of simulation runs, the direct output of
`run_simulation`.}

\item{idcol}{a character which names the column which enumerates the
simulation runs.  Default is "..simulation_run".}

\item{...}{Arguments passed to `confint.merMod`}

\item{progress}{A character vector:
- `"txt"` for a character based progress bar on the console
- `"shiny"` for a progress bar in the Shiny web interface
- `"none"` or NULL for not progress indication at all (default)}

\item{session}{Only in use when `progress = 'shiny'`.  Then you must supply
the shiny session object.}

\item{confints}{The direct output of `make_coverage_fixef_confints`}
}
\value{
a `data.table`

a ggplot
}
\description{
Compute the confidence intervals for each parameter in each
             simulation.

plot.mlSimFixefOutput
}
\details{
compute_fixef_confints
}
\section{Methods (by generic)}{
\itemize{
\item \code{plot}: `plot.mlSimFixefOutput` plots the
coverages of fixed effects
}}

